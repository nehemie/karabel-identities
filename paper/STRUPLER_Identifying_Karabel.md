# Being a Monument: The many selves of the Karabel relief

### Abstract

The role and memory of monuments in public space are currently hotly
debated. This shows that their contextualisation in the present is
necessary to let the public appreciate or criticize them. Monuments are
not born when they are crafted, but rather are the results of complex
processes when they are integrated in the social life of people. The
study of monuments should therefore not necessarily focus on the context
of their carving, but on how, where, when and why people interacted with
them. Despite this, archaeological research address only marginally the
multi-layered identity of a monument from discovery to interpretation.
To address this point and to investigate the process of being a monument,
a case study of the rock relief figure of Karabel is proposed. It
captures different contradictory lives that coexist to suggest a
monument of greater complexity and plurality, marked by wondering,
debate and damages.


### Submission comment

This manuscript was proofread and corrected by a native English speaker, but
minor changes were implemented afterwards. I hope that minor issues of
grammar and spelling can be overlooked during the initial submission
process. I would ensure a new proofreading after evaluation and
implementation of the reviewers comments (if my article is accepted).

Publication‐quality electronic images will be provided (all figures are CC-BY Licensed) and the 
bibliography will be formated according to the style guide.


### Conflict of interest

The author declares that there is no conflict of interest.

### Funding statement:

This work was supported by the [Name hidden for review] Grant.


## Introduction<!--[^acknowledgement]-->


> 'It is truly instructive to look upon a monument which has not
> only stood the decaying influences of thirty-three centuries,
> but more than this, has baffled for that space of time the
> human passions which have conspired to destroy it. It is an
> illustration of the aid rendered by remains of this nature
> toward establishing the statements of history as truths
> indisputable, and never to be shaken.' − [@lennep_1870_vol2,
> 322]

'Monuments have afterlives' claims Cherry and
encourages the exploration of how monuments are integrated in
the social life of people and how people interact with them by observing,
drawing, photographing, filming, remodelling, reusing, or damaging them
[@Cherry2013a, 1]. The prefix 'after' to the term 'lives' has been
interpreted as a way to emphasise the change of culture between
the time of the monument's creation and its later interactions with other people
[@Rojas2021_intro, 1]. However, to paraphrase Simone de Beauvoir,
monuments are not born when they are crafted, but rather becomes monuments.
The essence of monuments is that they only exist as an object of
other's interest [@oxford_monument]. Thus, monuments are only 'alive' when they
are integrated in the social life of people.

Monuments have been widely considered in multiple disciplines to reveal
memorial ambitions of the time of their creation or to have been a
site of contestation [@Nora1998_realms]. Moreover, the point has
clearly been made that monuments may be physically stable, but the
associated memories are highly mutable and targetable
[@Osborne2017_counter]. The physical destruction of monuments, most
frequently called 'iconoclasm', is documented in a vast variety of
places, at separate times and with a wide range of motivations
[@Gamboni_1997_destruction; @Clay_2012_iconoclasm_monography;
@Spicer2017a]. In recent decades, a remarkable effort has been made to
document how people have interacted with ancient monuments between their
conception and their modern rediscovery [@Ben-Dov2021_afterlives], even
if the recent past is still mostly eluded [@Rojas2021_intro, 23-24].
This trend of investigation sees monuments as touchstones which bring to
light not only collective, but also personal identity expressions that
are not documented in written or artefactual objects. By their presence,
monuments recall the past, are intended to be effective in the present,
but continue to be interpreted into the future. Costin has contributed
to the study of craft to reveal how the production process created value
and meaning for artifacts in societies. She emphasised that the study of
ancient craftsmanship provides not only a huge amount of information
about artisan identity and the role of objects and monuments to generate
identity, but also social, political, and spiritual power in the past
[@Costin1998_identity]. This paper took a similar course to examine how
the production process of the identity of a monument creates power and
meaning in societies, but starting from the discovery by modern European
scholars. This aims to better reflect on how ascribed identities mirror
personal worldviews following Fabian's assertion '[…] our ways of making
the Other are ways of making ourselves.' [@Fabian1990_representation,
756].

The introductory quote was taken from the biographical account of rev.
Henry J. Van Lennep (1815−1879). Its author reminisces about his
feelings when, in the 1860s, he faced the monument of Karabel, an
antique bas-relief situated today closed to Izmir (Turkey). Karabel had
become famous amongst western scholars 20 years earlier
[@lepsius_1840_Demonstration; see @Kohlmeyer1983_felsbilder]. Van
Lennep's mention of 'indisputable historical truths' refers to the
notorious work *the Histories* by the historian Herodotus. The work
associates the bas-relief with a representation of the pharaoh Sesostris
[@LLoyd1988a_commentary{pages 16-18, 313}; @west_1992_sesostris].
Herodotus attributed the identity of Sesostris to Karabel in order to
embed his discourse in a general historical framework and to create
authenticity and reality for Sesostris [@Haywood2021a;
@Sergueenkova2017a; @Rollinger2021a]. For Lennep and contemporaries,
this was proof of the authenticity of Herodotus' accounts about a
distant past [@Wees2002a_Herodotus_Past] − right at the moment when
scrambling for the past was in stiff emulation and even competition
[@Bahrani_2011_scramble]. Van Lennep utilises the existence of the
monument and Herodotus' identification to assert his own identification
and to bridge pristinely with the past after 33 centuries of 'decaying
influences'.
<!--, even if it was already
well known, that Sesostris was merely a fictional personnage
[@Zwingmann2013a].-->
By analysing the unique historiography of the relief of Karabel, we will
explore some of the different lives of the monument in the context of
their zeitgeist. Thanks to its geographical position at the interface of
many worlds, including Hittite, Greco-Roman and Modern Western,
Karabel provides a perfect opportunity to examine and untangle how
identities are evaluated and re-evaluated. Varied and contradictory
associations are shaped by institutional, ideological, aesthetic, and
national backgrounds [@Bohrer1998_inventing]. Particular focus will be
given to how the identity of the monument changed through time, starting
from its discovery from an for European scholars and look at major
shifts to the present.


## Becoming the monument of Karabel

<!--
>  […] representations ought to draw their convincingness
>  primarily from communication, rhetoric, and persuasion, and
>  only secondarily from systemic fits or logical proofs. The
>  logic of symbols, to name a conceptualization of knowledge
>  much in favor among anthropologists, is much more a matter of
>  Socratic persuasion in conversation than of Platonic appeal to
>  transcendent truths.  − [@Fabian1990_representation, 757]
-->

Beauvoir asserts that 'only the mediation of another can
establish an individual as an Other' [@Beauvoir1949{}]. Becoming
a monument is predicated on having a social life. In the case of
Karabel, we start by its discovery by western scholars, as it is
the starting point that still influences its widely acknowledged
identity today [@Strupler2023vandal]. Discovering a monument
related to an unknown past always sparks debate around its
identification, dating or meaning. A first step to
introduce a new monument is to convince the audience of its
authenticity. This is done by emphasizing the discovery process
and by claiming that the descriptions and drawings provided are
as accurate as possible.

Karabel was first rediscovered by oriental travellers in the early
19^th^ century [@lepsius_1840_Demonstration; @Lepsius1846_Karabel;
@Kiepert1843_sesostris; @Texier1849_bd2; review in
@Kohlmeyer1983_felsbilder]. There is dispute over precisely when Karabel
was discovered by (and for) western scholars, but it starts to be
frequently mentioned in travelogues from the 1830s onward. In the 1840s,
every article starts by explaining how the knowledge about the relief
came into (modern western) existence. It was of the utmost importance to
establish the knowledge transmission chain. Not only does identifying
the individual links of the knowledge transmission affirm the author's
authority, but providing further detail and rectification of previous
statements asserts the extent of the author's knowledge. When Karl
Lepsius 'revealed' the existence of the monument, he started his account
by listing the people who saw it and how he acquired his knowledge
[@lepsius_1840_Demonstration]. Subsequently, one further way of
evidencing crucial to proving the reality of the relief was to document
it with drawing. The process of obtaining the first drawing in order to
present it to an academy of science was convoluted. However, due to
Karabels place in the historiography of Hittites as part as the
authenticating process, it can be reconstructed with many details.

Lepsius learned about Karabel during a stay in London and
reported it to Humboldt in Paris in January 1838
[@Lepsius1846_Karabel, 271]. Humboldt asked the baron de Nerciat,
Dragoman of the French Consulat in Smyrna (Izmir), supposedly
through the Société de Géographie in Paris, to provide a drawing
of the monument. This request was passed to Texier. He drew the
figure<!--in 1839--> under commission to the Baron de Nerciat, who gave
it to Humboldt, with it finally arriving back to Lepsius shortly
after [@Lepsius1846_Karabel, 272; @Texier1849_bd2]. Lepsius did
not publish the drawing, but Texier published it himself
[@Texier1849_bd2]. In the initial publication, Lepsius provides
solely commentary to the drawing. He notes it shows Egyptian
clothing, but he also highlights inconsistencies with Egyptian
monuments such as the hieroglyphs form and the placement of the
inscription.


The rendering of Texier's drawing of Karabel (fig. 1) contrasts
sharply with his drawings from Yazılıkaya (fig. 2), even though the
Karabel drawing was made only slightly latter (1839), when Texier
visited Izmir. Today most people would concede that the style
of these reliefs are similar. However, there is no doubt that Texier was
influenced by the knowledge of Herodotus' reference that the image
represents an Egyptian Pharaoh when providing an image of and therefore
identity for Karabel (fig. 1). The influence of Egyptian style is
well visible in the clear definition of the contours depicted. In the
case of Yazılıkaya (fig. 2), even if the drawings are schematic and
fanciful, all the elements are much closer to the reality.



![Fig. 1. Texier's drawing of Karabel made in 1839 [@Texier1849_bd2]][drawing_texier_1849_1]

![Fig. 2. Texier's drawing of Yazılıkaya made in 1834 [@Texier1839_bd1]][drawing_texier_1839_1]

[drawing_texier_1839_1]: ./assets/img/Texier1839_Yazilikaya_1.jpg { width=50% }

[drawing_texier_1849_1]: ./assets/img/karabel_Texier1849.jpg { width=50% }

This inconsistency was quickly and widely criticised by his
contemporaries and later fellows. This was not an issue of the
general artistic ability of Texier. The drawing indicates his
need to adapt the reality to make it easily accessible. This was
probably a way to tame the past and to make it comprehensible.<!-- After
having visited the monument,--> Latter, Lepsius admitted that because the
relief corresponded to the description of Herodotus, there was no
reason to doubt that it was Egyptian. He even concedes that the
drawing was indeed required to prove the authenticity of
Herodotus' claims, not to question it [@Lepsius1846_Karabel,
271-272].

## Shaping Karabel

To make it a monument it was essential to give  to Karabel the correct
aspect. The transposition of Karabel to another medium (mainly drawing
for publications) reveals how its otherness was created. Although Texier's
drawing was the earliest, as it was not published for 10 years Heinrich
Kiepert has the claim of being the first academic to publish a drawing
of Karabel (fig.  3). He asserts that his provided descriptions and
drawings are as accurate as possible. For example, he concedes that any
drawing will have shortcomings:

[^kiepert_zeichnung]: *Die Figur […] ist sehr verwischt […] so
dass jede Zeichnung immer viel zu bestimmt ausfallen wird, idem
sie, um verständlich zu sein, auch das angeben muss, was sich nur
durch wiederholte genaueste Beschauung in grösster Nähe ergibt,
und beim Zurücktreten in eine Entfernung, aus der sich das Ganze
übersehen läßt, fast unsichtbar wird. In der beiliegenden
Abbildung, wenn dieser Vorwurf sie freilich auch trifft, ist
versucht worden, ein möglichst treues Bild zu geben, und
wenigstens nicht daring zugesetzt, oder angedeutet, was sich
nicht bei genauster Untersuchung als sicher ergab.*

> The relief […] is very blurred […] so that every drawing will always
> turn out to be too precise. To be understandable, the drawing must
> indicate details that only result from repeated exact observation in
> closest proximity, but when stepping back into a distance − from which
> the whole can be considered − they become almost invisible. Even if
> this reproach certainly applies to [my] illustration, an attempt has
> been made to give a picture as faithful as possible. At least nothing
> has been added or suggested that did not become certain upon close
> examination. [@Kiepert1843_sesostris,{39, tr. Strupler}]
> [^kiepert_zeichnung]

![Fig. 3. Kiepert's drawing of Karabel [@Kiepert1843_sesostris]][drawing_kiepert_1843_full]

[drawing_kiepert_1843_full]:  ./assets/img/karabel_Kiepert1843.jpg  { width=50% }

Kiepert was the first scholar to question the identity of Karabel and
the assumption that it was an (ancient) Egyptian representation
[@Kiepert1843_sesostris; @west_1992_sesostris]. He highlights the
differences between the style of the Karabel figure and ancient Egyptian
art. He perspicaciously proposes that although the bas-relief may
conform to Herodotus' physical description, Herodotus may have been
incorrect in identifying it as a pharaoh. Kiepert observes that the
signs are not Egyptian hieroglyphs. Instead, he associates the relief
with the drawings from Yazılıkaya that Texier made available in his
first travelogue volume [-@Texier1839_bd1].

In turn, Lepsius, who visited the monument shortly after Kiepert's
publication, criticises the drawing and the interpretation of elements
considered significant in determining the identity of Karabel. He
calls attention to the discrepancies between the reality and Kiepert's drawing such
as, the cut of the garment at knee height, the invention of a frame that
encircles the hieroglyphic signs − and the fact that Kiepert speaks
about an (Egyptian like) cartouche. He also asserts that the form of the
head cap and hieroglyphic signs, or the weapon extremities are malformed
in Kiepert's drawing. Lepsius made his own drawing (fig. 4), but it
was modified for the subsequent publication. Under the publication of
Lepsius' image, the editor acknowledges that the drawing is there only
to show the differences from the earlier published Kiepert
rendering, but does not explain why Lepsius
drawing was considered as less trustworthy:<!--[^lepsius-archiv]-->


> 'The following woodcut serves only to clarify the differences
> [with Kiepert's drawing], while for the overall impression of
> the monument, especially regarding the chest and body of
> the figure, [Kiepert's Drawing] retains the advantage of
> greater fidelity.' [@Lepsius1846_Karabel,
> 273, tr. Strupler] [^lepsius-drawing-caption]


[^lepsius-drawing-caption]: '*Lediglich zur Verdeutlichung dieser
 Abweichungen dient der beifolgende Holzschnitt , während für
den Gesammteindruck des Monuments, namentlich in Bezug auf Brust
und Leib der Figur, die von uns (Taf . III) gegebene Zeichnung
den Vorzug grösserer Treue behält . A. d. H.*'


![Fig. 4. Drawing from @Lepsius1846_Karabel][drawing_lepsius_1846_full]

[drawing_lepsius_1846_full]:  ./assets/img/karabel_Lepsius1846.jpg  { width=50% }

Lepsius' revision, drawing and criticism are only one example
from a vast body of literature. The 'Hittite style', to which we
attribute the relief today was then unknown
[@muller-karpe_2019_herodot], and knowledge about the
pre-classical past was limited – speculation was plentiful and
ongoing. For Lepsius, Karabel represented the pharaoh Sesostris,
to explain the stylistic difference with Egyptian monuments and
his 'confusing identity', he postulates that Karabel was realised
out of necessity 'in the absence of Egyptian artists' by
'barbaric hands' [@Lepsius1846_Karabel, 274]. The architect
Pierre Trémaux (1818–1895) like Lepsius, traveled to Karabel
after a trip to Egypt. He provides a plate combining drawing and
photographs as well as text for his series documenting the
Anatolian region [fig. 5, @Tremaux1858_exploration{Nymphaeum, pl.
1}; further see @Addleman-Frankel2018]. Trémaux calls Karabel the
'Monument of Sesostris after Herodotus', but considers it
genuinely Egyptian, though from a 'decadent' style. Curiously, he
states that the drawing was necessary to show detail that 'cannot
be seen' on a photograph, but also that the photography was
necessary to record the fuzziness of the monuments, warning that
due to the terrain, the photography had to be taken from an
angle.

<!--
[^lepsius-archiv]: Enquiry at the Archivmaterial der Lepsius-Expeditionesearch Archivmaterial der Lepsius-Expedition
-->

![Fig. 5. Photo and drawing from @Tremaux1858_exploration][drawing_tremaux_1858_full]

[drawing_tremaux_1858_full]:  ./assets/img/karabel_Tremaux1858.jpg  { width=50% }
<!--With these publications ,
.-->

The drawing below Trémaux' picture seems a blend of the drawings of
Texier, Kiepert and Lepsius. The figure is slim and 'Egyptian' as
in the Texier illustration, but the details of the garments are
closer to Kiepert's and Lepsius' images. Surprisingly, even
though Trémaux aimed to familiarise western audiences with
distant places [@Ryan2013], this photograph did not receive
broad recognition. One of the rare citations of this work is from
Salomon Reinach, who complains about the quality of Trémaux'
works [@Reinach1888_voyage, 45]. Reinach's own interests were in
art history. He published a new, more artistic drawing of the
relief made by the illustrator Eugène Landron (fig. 6). There are no
details about how the drawing was made, but the lively three
dimensional nature of the representation is more artistic than
factual. It is closer to a naturalistic style than the *in situ*
flat surface of the physical reliefs, characteristic of Hittite
and Late Hittite [@Orthmann1971_untersuchungen]. However,
Reinach is more cautious when commenting on the relief and
considering the style. Indeed, Sayce had just made the
proposition that Karabel was a Hittite relief, until then a
civilization exclusively known from sporadic mentions in
biblical, Egyptian and Assyrian sources and which was only just
starting, little by little, to be understood.
[@Sayce1879_Academy; @sayce_1880_forgotten; @Tyler_1888_Nature_b;
see the contextualization by @alaura_2017_setting].

![Fig. 6. Drawing from @Reinach1888_voyage][drawing_reinach_1888_full]

[drawing_reinach_1888_full]: ./assets/img/karabel_Reinach1888.jpg  { width=50% }

This introductory comparison of the differences in the drawings
illustrates the way *seeing* and *drawing* Karabel was inevitably
strongly associated with the *identity* attached to it. Early
representations of the monument were made with the purpose of rhetoric
and persuasion in mind, rather than accuracy. It is only after various
attempts, debates, reiterating visits and gradually putting aside
preconceptions that it became possible to produce an accurate drawing
of the 'ambiguous' style and identity of the relief. From thereon, with
the general acceptance that Karabel was not a representation from
Sesostris, but a Hittite effigy, scholars argued widely over the
'correct' identity of the person represented. Crucially, in a period
where 'race' was one of the most hotly debated topics.^[In case
of doubt, I would like to recall that racism exists, not race.]

## Racialization Sesostris' portrait into a Hittite King


In 1879, Sayce travelled to Karabel. After telling the history of
the discovery and 'naturally' complaining about Texier's drawing
his account states: 'Now that the facsimile [i.e. Sayce's drawing] has been obtained, we have positive proof that the race
which produced the sculpture of Karabel […] was the race called
Hittites in the Old Testament' [@sayce_1880_forgotten, 225].
Sayce advanced that the pointed shoes indicated that Hittites
'came from a cold country such as the highlands of Armenia'
[@sayce_1880_forgotten, 232]. By recognising similarity with
characters on monuments and inscriptions in Asia Minor, Sayce
raised the question of the identity of the 'Hittite race', a
critical issue at this time [@alaura_2017_little, 20]. Dominated
by colonial ideology [@Alaura2002_prima], 'oriental' scholars
were heavily concerned with localising peoples and nations
mentioned in the Bible as well as in classical texts
[@weeden_2017_before, 127]. The idea of race, which can not be
rightfully addressed here, is complex, ambiguous and changes
through times and perspectives. In many cases, for scholars of
that time, 'race' was heavily influenced by linguistic as well as
anthropological consideration: 'The word 'race' is used in
different senses by men [sic] of science and ordinary people. The
scientific ethnologist […] classifies principally by
physiological factors, such as shapes and proportions of skulls
[…but otherwise] men [sic] can be classified by their language,
and in this sense we can talk of the Latin, Teutonic, or Slav
race.' [@Toynbee1918_races]. The obsessions of racial
categorization was supported by many research, such as W. Z.
Ripley's 1899 widely acclaimed book *The Races of Europe* [see
@Heather2006_ripleys-races].

Scholars were therefore more interested in looking at Karabel as
part of the wider consideration of attributes in common of 'the
race', rather than singularising it and investigating it's
individual history. For example, when Felix von Luschan
excavated at the Late Hittite city of Sam'al (Zincirli, Turkey),
he also conducted anthropometrics research on local people to
distinguish races based on the form of the head and language. He
then applied his research to reconstruct past history based on
'likeness' with types depicted on the bas-reliefs unearthed at
Zincirli [@Luschan1894_jews; @SmithJD2002_felix_von_luschan].
Similarly, monuments in Egypt depicting 'the Hittites' were used
to see the correlation with Hittite representation, focusing on
'typical' garments, hairstyle or body shapes as stated by Claude
Conder and followed by many [such as @Sayce1882_Monuments;
@wright_1884_empire_hitites, 86; @Tyler_1888_Nature_a, 515; or
@hirschfeld_1887_felsenreliefs, 49-58]:

> 'In the picture of the Ramesseum Kadesh […] the Hittites and
> their allies are represented as distinct races with different
> kinds of weapons. The one race is bearded, the other beardless,
> and in the Abu Simbel representation, the Chinese-like
> appearance of the Hittites, who have long pig-tails, is very
> remarkable' [@Conder1883_Heth_Moab_1882, 22−23]

During this pioneering period of continuous discoveries of
ancient monuments, most were lumped into larger stylistic and
geographical groups in order to organise and classify them
(Messerschmidt 1900). Following the discovery of cuneiform
tablets at Amarna (Egypt) in 1887, the 1905−1912 excavations in
Boğazköy, revealed and made available thousands of clay tablets
written in the yet unintelligible 'Hittite' language. This
discovery led to more research into 2nd Millennium BC in
Anatolia. Most of this research was dedicated to the decipherment
of the language in cuneiform and hieroglyphic writing. Debates
revolved around the origin of the language and written system,
arguing for and against an origin in 'Semitic', 'Indo-European',
'Turanian', 'Scythian', or 'Sumerian' languages (and 'race'). As
pointed out by Alaura [-@Alaura2015_identity], similarly to the
'Sumerian problem' and its supposed 'race'
[@Cooper1993_sumerian], scholars spoke about the 'Hittite
problem'. When Hogarth explained the reason behind the start of
the Carchemish excavations (Syrian/Turkish border), he
unmistakably formulates the critical issue of that time: 'what
part, if any, did the Hittites play in the general development of
European civilization out of Asiatic?' [@Hogarth1911_hittite, 1].
When in 1915 Hrozný called his article 'The Solution to the
Hittite Problem', it was to announce the decipherment of the
Hittite language [@Hrozny1915]. Hrozný's categorically
demonstrated that Hittite was an Indo-European language. However
this was not accepted by everyone at the outset, precisely
because of the implications for identity and race that it
presented. To put it bluntly, for some, it was not palatable to
accept that 'Indo-Europeans' have 'Chinese alike' appearance
[further see @Alaura2015_identity, 27].


## A Monument with dedication. Reading Karabel's Inscription

Rapidly, huge progress was made in reading and understanding the
state archive at Boğazköy. From the 1930s, hieroglyphic signs
started to be better understood [@Hawkins2000a, 13−17], but in
the case of the short and eroded Karabel inscription, it took 50
years for the debate to settle down [see
@Kohlmeyer1983_felsbilder, 16‐9, 25-28;
@hawkins_1998_tarkasnawa<!--;
also @schachner_2018_bericht, 60-65-->]. During this period, the
hieroglyphic system was gradually better recognized, analysed and
understood. As long as the inscription was not fully understood,
the relief of Karabel was not considered separately and only
analysed with the other monuments to compare similar sign
sequences.


Accordingly, based on the comparison with Yazılıkaya and Gavurkalesi,
Garstang considered Karabel to be the representation of the 'Hittite
national deity' [@Garstang1910_exploration, 173; again twenty years
later @Garstang1929_hittites, 178]. In 1931, Sayce proposed the
following (intrepid) reading 'Monument of Tuatis, the king of the
country of Khalmis, the High priest' [@Sayce1931_Karabel, 430]. Bittel
was more prudent in his interpretation, considering that it was only
possible to read the sign 'King' with some certainty without knowing if
this refers to a representation of a king or a dedicatory person, and
cautiously proposed the King Tutḫaliya (IV) due to the style and a
partly conserved sign [@Bittel1939_karabel, 182]. Bossert tried in 1946
[@Bossert1946_asia, 72-75] and agrees with Bittel in the reading of the
'King' sign, but opinionated that it could be a local King because the
name could not be associated with known Hittite King [a guess that will
be proved true, @hawkins_1998_tarkasnawa]. Without conclusive proof,
researcher were arguing for the one or other solution, as well as for
the dating of the relief in the 14th or 13th century BC [see
@Kohlmeyer1983_felsbilder, 25-26]. For example, Bittel revised his
position in stating that it was either a member of the royal family or a
local ruler [@bittel_1967_karabel], and Kohlmeyer who did a thorough
review designates Karabel solely as 'ruler' [-@Kohlmeyer1983_felsbilder,
25-26]. The debate changed drastically with the reading of the
inscription. The considerable amount of new discoveries and advances in
the decipherment of hieroglyphic readings proved to be decisive. Hawkins
states : 'I was again pondering the reading of Karabel [… and] with my
mind very much on our reading of *Tarkasnawa*, I suddenly saw that this
is exactly what is on Karabel' [@hawkins_1998_tarkasnawa, 4,
emphasis in original]. His
statement indicate undeniably that to *see* the name, he needed to already
have something in his mind about the relief. Suddenly, Hawkins reached the
starting point by giving back an individual life to Karabel. Like
Sesostris, Karabel is not an anonymous ruler, but a person named in
other documents, allowing us to grasp some elements of his biography.
Since this reading, the relief is considered as the
representation of Tarkasnawa, king of Mira, a vassal to the
Hittite King [@hawkins_1998_tarkasnawa; @muller-karpe_2019_herodot;
@gander2022, 488-495].

## Some non-academic lives


![Fig. 7. Photography from Karabel taken in 1907 by Gertrude Bell. Gertrude Bell Photographic
Archive n°F\_015, Newcastle University, <http://www.gerty.ncl.ac.uk/images/F_015.jpg>](./assets/img/Bell_F_015.jpg){#fig7
width=45%}



During the 19^th^ century, the first steps to recognise Karabel as
Hittite were made. The monument was compared with the other Hittite
monuments and integrated into the bigger picture of the Hittite Kingdom
and its changes through time. Karabel regained a distinct identity as a
specific king based on  accumulated evidence after having lost his
Sesostris name and pharaoh title. For some people, this would be the end
of the research into Karabel's (after)lives.  However, I would like to
challenge that by this path alone we know all the relevant elements
about the lives of Karabel. Arguably, not many research papers deal with
the perception by the contemporary public and visitors. This silences
the fact that Karabel is not only a reminder of historical events, but
it is also a place of folklore. It must have been a theme of oral
discussions by people visiting the place and by nearby communities.
One of the first 'modern' evidence of interaction with Karabel can be
seen on a picture taken in 1907 by Gertrud Bell ([fig.  7](#fig7)).  The
picture shows inscriptions on and next to the relief, mainly written in
Greek letters.  If this sounds trivial today and many archaeological
sites advertise against this practice, it is probably a
development of the last two centuries [@iranica_graffiti]. It undeniably
shows that it was a known place for communities living close by.
Moreover, there is no indication of graffiti on the first photos of the
monuments and seems correlated with the re-discovery of Karabel by
western scholars.



![Fig. 8. Photograph of the relief of Karabel taken in 2019 by the
author. Extensive damages are clearly visible on the legs and
the middle of the relief, accentuated by a drilled hole.](./assets/img/2019-09-26_Karabel.jpg){#fig8 width=45% }

People who visited the monument in the last 50 years could witness that
it was damaged at multiple times ([fig. 8](#fig8)). This attests that
the identity and historical uniqueness of Karabel as depicted in content
of most of the scholars articles were contested [@Strupler2023vandal].
Researcher prefers to use a picture of a 'pristine' condition rather
that the actual state, which blatantly exposes our failure to protect
it.  Recently, Karataş conducted ethnographic research among looters in
the black sea region to explore the relations between archaeological
remains and hodjas (self-proclaimed spiritualists) and how they advise
and contribute to the search and explanation of mythical signs for
treasure hunting [@Karatas2022]. She shows how folklore and oral
traditions justify looting.  Similar explanation may be suggested for
Karabel, as part of the life of Karabel, even if this folklore
contributes to 'tolerate the irreversible damage being done by the local
communities involved' [@Karatas2022, 293; see also @Yolcu2017]. However,
even before these degradations, important destruction
already took place, when the state financed the enlargement of the road
passing through the Karabel pass. Between 1977 and 1982, this public work demolished closely
situated reliefs [@Kohlmeyer1983_felsbilder]. Sadly, the
last monument of Karabel suffered heavy damages in 2019 [@masrop_2019_karabel]. The
destruction and damage are not only attributable to treasure hunting
([fig. 8](#fig8)). The monument was smashed with hammers to destroy the
motif. It has also been reported that acid was poured on the relief
[@Baykan2013]. This harm of the last 50 years calls on the
archaeological community to better understand and document the current
life of this monument. After a hundred years since Van Lennep's
statement, we should ask ourself what is the responsibility of modern
research and our social environment, if during 33 centuries of 'decaying
influences' Karabel stayed intact [@lennep_1870_vol2, 322], but
circumstances changed mainly after western scholars discovered it (for
themselves).


# Conclusion

This paper considers how the different lives of Karabel create value and
meaning in societies. It examines how different identities were forged to
a stone monument, beginning with the western scholars' exploration.
From the first scholar mention to its authoritative reading, the relief
was alternately considered to be a representation of the pharaoh
Sesostris, a fictional character in Herodotus' *Histories*, a local
ruler, a Hittite King and finally, Tarksnawa, a vassal of the Hittite
King. It is an important monument to discuss the geographical-history of
western Asia Minor in the second millennium BC [@glatz_2011_lanscape;
@meric_2021_arzawa; @seeher_2009_landschaft], and the discussion about
his interpretation is still ongoing [@gander2022, 488-495]. To
understand a monument, however, involves more than its role in the past.
It should consider how it was received and it should be the occasion to reflect
what we would like it to mean for the future. The case of Karabel and
its racial identity nicely
illustrates how our preconceptions instantiate the identity of monuments:

>  […] representations ought to draw their convincingness
>  primarily from communication, rhetoric, and persuasion, and
>  only secondarily from systemic fits or logical proofs. The
>  logic of symbols, to name a conceptualization of knowledge
>  much in favor among anthropologists, is much more a matter of
>  Socratic persuasion in conversation than of Platonic appeal to
>  transcendent truths.  − [@Fabian1990_representation, 757]

In the US and elsewhere a wave of statue removals is taking
place (the so-called 'Statue War'). This sparked a debate about what are the meaning of
monuments and how we should view them today [@Atuire2020;
@Campbell2017]. It shows that disentangling the different meanings of a
monument is highly delicate. However, whatever happens to a monument,
there is an agreement to better contextualise monument, in situ or after
removal [@Antonello2021]. The essential work of archaeologists and
historians is not only to explain why monuments were erected, but also
to recognize that once erected, statues are focus of new stories and
representations. The narrative of any monument should include both its
creation and its ongoing history, destruction, or alteration, as Young
argued for memorials [@Young1993]. Antonello propose the idea of
're-storying monuments' to unearth lost histories 'in the context of
current concerns around racial and social justice, climate change and
the Anthropocene and eroding working rights and conditions'
[@Antonello2021, 747]. For this, we need to add new stories to ask which
future we can build. One of the lives that I prefer for Karabel is the debate about how we could
protect it and how the conservation plan proposed by Baykan could be
implemented [@Baykan2013]. This would give us the opportunity to put
the urgence to protect archaeological sites and natural environment in the foreground.



## Acknowledgements
<!--[^acknowledgement]: --> This paper is an outcome of my talk at the [name
    hidden for review]<!--TAG-Türkiye--> conference 2021<!-- 'Kimlikler
/ Identities'-->, and I also recently submitted a specific paper on the
entanglement between Karabel and orientalism [@Strupler2023vandal]. For this
paper, I am grateful to [name hidden for review] for providing comments
on drafts of this article at multiple stages. [Name hidden for review]
contributed with editing the English and provided insightful comments.
<!--Following the fruitful TAG-Türkiye conference in 2021, I published a
paper on the relation of Karabel and Orientalism (name hidden for
review).--> I would like to thank the organizers, the participants and
the public for there inputs throughout the [name hidden for
review]<!--TAG-Türkiye 2021--> conference<!--and the editors for their
patience, organizing its translation from English to Turkish-->.  <!-- I
would like to thank the comments from the reviewers.-->This work was
supported by the [names of institution hidden for review] <!--SNF,
allowing me to be hosted at The McDonald Institute for Archaeological
Research in Cambridge--> and I would like to thank these institutions
for providing an ideal environment.



## References {-}

<!--
### Gallery {#Gallery .gallery}

![Drawing from @Texier1849_bd2][drawing_texier_1849]

![Drawing from @Schmitz1844_sesostris][drawing_schmitz_1844]

![Drawing from @Kiepert1843_sesostris][drawing_kiepert_1843]

![Drawing from @Lepsius1846_Karabel][drawing_lepsius_1846]

![Photo and drawing from @Tremaux1858_exploration][drawing_tremaux_1858]

![Drawing from @Ritter1858_asien][drawing_ritter_1858]

![Drawing from @Moustier1864_tour][drawing_moustier_1865]

![Photo from @Perrot1866_nymphi][drawing_perrot_1866]

![Photo from @Weber1880_sipylos][drawing_weber_1880]

![Photo from @Sayce1882_Monuments, 266-267][photo_sayce_1882]

![Drawing from @Sayce1882_Monuments,267][drawing_sayce_1882]

![Drawing from @Reinach1888_voyage][drawing_reinach_1888]

![Drawing from @Messerschmidt1900_corpus, plate 39][drawing_messerschmidt_1900]

![Drawing from @Garstang1910_exploration][drawing_garstang_1910]

![Drawing from @Garstang1929_hittites][drawing_garstang_1929]









[drawing_kiepert_1843]:  ./assets/img/karabel_Kiepert1843.jpg  { width=32% }

[drawing_schmitz_1844]:  ./assets/img/karabel_Schmitz1844.jpg  { width=32% }

[drawing_lepsius_1846]:  ./assets/img/karabel_Lepsius1846.jpg  { width=32% }

[drawing_texier_1849]: ./assets/img/karabel_Texier1849.jpg { width=30% }

[drawing_ritter_1858]:  ./assets/img/karabel_Ritter1858.png  { width=32% }

[drawing_tremaux_1858]:  ./assets/img/karabel_Tremaux1858.jpg  { width=32% }

[drawing_perrot_1866]:   ./assets/img/karabel_Perrot1866.jpg  { width=32% }

[drawing_moustier_1865]: ./assets/img/karabel_Moustier1865.jpg  { width=32% }

[drawing_weber_1880]:    ./assets/img/karabel_Weber1880.jpg  { width=32% }

[photo_sayce_1882]:    ./assets/img/karabel_Sayce1882.jpg  { width=32% }

[drawing_sayce_1882]:    ./assets/img/karabel_Sayce1882b.jpg  { width=32% }

[drawing_reinach_1888]:  ./assets/img/karabel_Reinach1888.jpg  { width=32% }

[drawing_messerschmidt_1900]: ./assets/img/karabel_Messerschmidt1900.png  { width=32% }

[drawing_garstang_1910]: ./assets/img/karabel_Garstang1910.jpg  { width=30% }

[drawing_garstang_1929]: ./assets/img/karabel_Garstang1929.jpg  { width=32% }


## A forgotten forgotten Monument, decaying to destruction?


-->



<!--
> The following paper will be one rather of conjecture than of
> facts; but where facts are not attainable, even conjecture have
> their use [@Sayce1877,22]
-->

<!-- Karabel missing from scrapbook[^Henry John Van Lennep
scrapbook, in Henry J. Van Lennep (AC 1837) Sketches and Papers
Box 1 Folder 17, Amherst College Archives and Special
Collections, Amherst College Library.
https://archivesspace.amherst.edu/repositories/2/resources/187
Accessed January 13, 2022.]

https://medium.com/@saraduell/confronting-visual-narratives-of-white-supremacy-45984ecca4ee
-->

